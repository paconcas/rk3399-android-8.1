###############################################################################
# FMODV1.0.0
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_OVERRIDES_PACKAGES := Home Launcher2 Launcher3 Launcher3QuickStep Launcher3Go
LOCAL_MODULE := EmptyLauncher
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := EmptyLauncher
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true // if required
LOCAL_MODULE_CLASS := APPS
LOCAL_SRC_FILES := EmptyLauncher.apk
include $(BUILD_PREBUILT)

