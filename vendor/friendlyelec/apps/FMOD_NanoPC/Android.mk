###############################################################################
# FMODV1.0.0
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_OVERRIDES_PACKAGES := Home Launcher2 Launcher3 Launcher3QuickStep Launcher3Go
LOCAL_MODULE := fmod-1.0.0
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := fmod-1.0.0
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true // if required
LOCAL_MODULE_CLASS := APPS
LOCAL_SRC_FILES := fmod-0.0.1.apk
include $(BUILD_PREBUILT)

